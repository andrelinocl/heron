<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico">

    <title>CabHost</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="css/hover.css" rel="stylesheet" media="all">
    <link href="css/responsive.css" rel="stylesheet" media="all">
    <link href="css/animate.css" rel="stylesheet" />

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-offset="80">
<!-- PREELOADER START -->
<div class="se-pre-con">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- END PREELOADER -->

<!-- BEGIN BODY -->
<header>
    <div id="particles-background" class="vertical-centered-box"></div>
    <div id="particles-foreground" class="vertical-centered-box"></div>
    <div id="clouds-background"></div>
    <nav class="navbar full-nav main-header">
        <!-- BEGIN NAVBAR HRADER -->
        <div class="container">
            <!-- BEGIN CONTAINER -->
            <div class="navbar-header">
                <!-- RESPONSIVE MENU BEGIN -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.html#"><img src="img/logo.png" alt="" /></a>
            </div>
            <!-- END RESPONSIVE MENU -->
            <div id="navbar" class="navbar-collapse collapse">
                <!-- BEGIN NAV LINK -->
                <ul class="nav navbar-nav navbar-right">
                    <li><a data-scroll href="../index.html#fist-section">Lorem</a></li>
                    <li><a data-scroll href="../index.html#third-section">Ipsum</a></li>
                    <li><a data-scroll href="../index.html#fourth-section">Dolor</a></li>
                    <li><a data-scroll href="../index.html#fourth-and-half-section">Amet</a></li>
                    <li><a data-scroll href="../index.html#our-place">Lorem</a></li>
                    <li class="mobile-center">
                        <a style="padding-right:0 !important;padding-left:0 !important;" class="chat-brn" href="../index.html#"> <span>live chat </span><img src="img/chat.png" alt="" /></a>
                    </li>
                    <!-- LIVE CHAT BUTTON -->
                </ul>
            </div>
            <!-- END NAV LINK -->
        </div>
        <!-- END CONTAINER -->
    </nav>
    <!-- END NAVBAR HEADER -->

    <div class="header-top-titte">
        <!-- HEADER BODY BEGIN -->
        <div class="container">
            <!-- BEGIN CONTAINER -->
            <h5 class="wow fadeIn" data-wow-delay="0.6s">one of the best <b>hosting</b> providers in the world.</h5>
            <h5 class="wow fadeIn" data-wow-delay="0.8s">Start using our services now.</h5>

        </div>
        <!-- END CONTAINER -->
    </div>
    <!-- END HEADER BODY -->
</header>
<div id="domain-search">
    <!-- BEGIN DOMAIN SEARCH -->
    <form>
        <input type="text" name="domain" value="yourdomain.com" />
        <button class="srch-btn wow fadeIn" data-wow-delay="0.3s"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
</div>
<!-- END DOMAIN SEARCH -->

<section id="fist-section">
    <!-- FIRST SECTION BEGIN -->
    <div class="container">
        <!-- BEGIN CONTAINER -->
        <div class="col-md-push-1 col-md-10 col-xs-10 tablet-full-width">
            <div class="homepage-tabs">
                <div class="row">
                    <!-- BEGIN ROW -->

                    <ul role="tablist">
                        <!-- BEGIN TABS LIST -->
                        <li role="presentation" class="col-md-3 active wow fadeIn" data-wow-delay="0.1s">
                            <a href="../index.html#protection" aria-controls="protection" role="tab" data-toggle="tab"><i class="flaticon-shield-5"></i> <span>High protection</span></a>
                        </li>
                        <li role="presentation" class="col-md-3 wow fadeIn" data-wow-delay="0.3s">
                            <a href="../index.html#hosting" aria-controls="hosting" role="tab" data-toggle="tab"><i class="flaticon-database-17"></i> <span>web hosting</span></a>
                        </li>
                        <li role="presentation" class="col-md-3 wow fadeIn" data-wow-delay="0.5s">
                            <a href="../index.html#Hardware" aria-controls="Hardware" role="tab" data-toggle="tab"><i class="flaticon-chip"></i> <span>Best Hardware</span></a>
                        </li>
                        <li role="presentation" class="col-md-3 wow fadeIn" data-wow-delay="0.7s">
                            <a href="../index.html#Management" aria-controls="Management" role="tab" data-toggle="tab"><i class="flaticon-laptop-2"></i> <span>Professional Management</span></a>
                        </li>
                    </ul>
                    <!-- END TABS LIST -->

                </div>
                <!-- END ROW -->
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
</section>
<!-- END FIRST SECTION -->

<!-- BEGIN TABS CONTANT -->
<section id="second-section">
    <!-- BEGIN SECOND SECTION -->
    <div class="container">
        <!-- BEGIN CONTAINER -->
        <div class="tab-content col-md-12">
            <div role="tabpanel" class="tab-pane fade in active" id="protection">

                <div class="tabs-text wow fadeIn" data-wow-delay="0.3s">
                    <span>Lorem ipsum dolor sit amet,</span> consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                    <a class="df-bttn bluebk wow fadeIn hide" data-wow-delay="0.5s" href="../index.html#">Read more</a>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="hosting">

                <div class="tabs-text">
                    <span>Lorem ipsum dolor sit amet,</span> consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                    <a class="df-bttn bluebk hide" href="../index.html#">Read more</a>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="Hardware">

                <div class="tabs-text">
                    <span>Lorem ipsum dolor sit amet,</span> consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                    <a class="df-bttn bluebk hide" href="../index.html#">Read more</a>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="Management">

                <div class="tabs-text">
                    <span>Lorem ipsum dolor sit amet,</span> consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                    <a class="df-bttn bluebk hide" href="../index.html#">Read more</a>
                </div>

            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
</section>
<!-- END SECOND SECTION -->

<section id="third-section">
    <!-- BEGIN THIRD SECTION -->
    <div class="container">
        <!-- BEGIN CONTAINER -->
        <div class="row">
            <!-- BEGIN ROW -->
            <div class="col-md-12">
                <div class="tittle-page">
                    <!-- TITTLE -->
                    Lorem ipsum dolor sit amet.
                    <span>A deleniti doloribus exercitationem nulla repellendus, voluptas. Atque blanditiis delectus eaque eveniet ipsam iste iusto.</span>
                </div>

                <div class="row">
                    <!-- START ROW -->
                    <div class="col-md-10 col-md-push-1">
                        <div class="col-md-4">
                            <div class="pricing-table wow fadeIn" data-wow-delay="0.4s">
                                <!-- START PRICING TABLE -->
                                <div class="tittle-pricing">
                                    Free
                                    <span></span>
                                </div>
                                <div class="amount-pricing">
                                    <span class="currency">$</span>
                                    <span class="amount">12.99</span>
                                </div>
                                <div class="table-fetures">
                                    <ul>
                                        <li><span class="flaticon-database-17"></span> 10gb Storage</li>
                                        <li><span class="flaticon-chip"></span> 16gb ram</li>
                                        <li><span class="flaticon-shield-5"></span> - </li>
                                    </ul>
                                    <a class="dff-bttn greenbk" href="../index.html#">Read more</a>
                                </div>

                            </div>
                            <!-- END PRICING TABLE -->
                        </div>

                        <div class="col-md-4">
                            <div class="pricing-table pricing-table-active">
                                <!-- START PRICING TABLE -->
                                <div class="tittle-pricing">
                                    Agência
                                    <span></span>
                                </div>
                                <div class="amount-pricing">
                                    <span class="currency">$</span>
                                    <span class="amount">15.99</span>
                                </div>
                                <div class="table-fetures">
                                    <ul>
                                        <li><span class="flaticon-database-17"></span> 10gb Storage</li>
                                        <li><span class="flaticon-chip"></span> 16gb ram</li>
                                        <li><span class="flaticon-shield-5"></span> Full protection</li>
                                    </ul>
                                    <a class="dff-bttn greenbk" href="../index.html#">Read more</a>
                                </div>

                            </div>
                            <!-- END PRICING TABLE -->
                        </div>

                        <div class="col-md-4">
                            <div class="pricing-table wow fadeIn" data-wow-delay="0.6s">
                                <!-- START PRICING TABLE -->
                                <div class="tittle-pricing">
                                    Coroporação
                                    <span></span>
                                </div>
                                <div class="amount-pricing">
                                    <span class="currency">$</span>
                                    <span class="amount">19.99</span>
                                </div>
                                <div class="table-fetures">
                                    <ul>
                                        <li><span class="flaticon-database-17"></span> 10gb Storage</li>
                                        <li><span class="flaticon-chip"></span> 16gb ram</li>
                                        <li><span class="flaticon-shield-5"></span> Full protection</li>
                                    </ul>
                                    <a class="dff-bttn greenbk" href="../index.html#">Read more</a>
                                </div>

                            </div>
                            <!-- END PRICING TABLE -->
                        </div>

                    </div>
                </div>
                <!-- END ROW -->
            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->

    <div class="do-area hide">
        <!-- START ORDER NOW AREA -->
        <div class="container">
            <!-- BEGIN CONTAINER -->
            <div class="row">
                <!-- BEGIN ROW -->
                <div class="col-md-8 col-md-push-2">
                    <div class="col-md-8 wow fadeIn" data-wow-delay="0.2s">
                        our services is the best choice for you
                    </div>

                    <div class="col-md-4">
                        <a class="rg-bttn bluebk wow fadeIn" data-wow-delay="0.4s" href="../index.html#">order now</a>
                        <!-- ORDER NOW BUTTON -->
                    </div>
                </div>
            </div>
            <!-- END ROW -->
        </div>
        <!-- END CONTAINER -->
    </div>
    <!-- END THE AREA -->
</section>
<!-- END THIRD SECTION -->

<section id="fourth-section">
    <!-- FOURTH SECTION -->
    <div class="container">
        <!-- START CONTAINER -->
        <div class="tittle-page">
            <!-- TITTLE -->
            O que ofertamos
            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A animi deleniti eaque.</span>
        </div>
        <div class="row">
            <!-- START ROW -->
            <div class="col-md-12">
                <div class="col-md-3 conter-align spcl-offr wow fadeIn" data-wow-delay="0.1s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-shield-5"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

                <div class="col-md-3 conter-align spcl-offr wow fadeIn" data-wow-delay="0.3s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-database"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

                <div class="col-md-3 conter-align spcl-offr wow fadeIn" data-wow-delay="0.5s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-internet"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

                <div class="col-md-3 conter-align spcl-offr wow fadeIn" data-wow-delay="0.7s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-laptop-8"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

            </div>
        </div>

        <div class="row mrgntopbbr">
            <div class="col-md-10 col-md-push-1">
                <div class="col-md-4 conter-align spcl-offr wow fadeIn" data-wow-delay="0.9s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-printer-2"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

                <div class="col-md-4 conter-align spcl-offr  wow fadeIn" data-wow-delay="1.1s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-settings"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

                <div class="col-md-4 conter-align spcl-offr wow fadeIn" data-wow-delay="1.4s">
                    <div class="hvr-ripple-out spcl-services">
                        <i class="flaticon-server"></i>
                        <!-- ICON -->
                    </div>
                    <h5>WEBSITE HOSTING</h5>
                    <!-- TITTLE -->
                    <p>we want you to be happy width our services</p>
                    <!-- TEXT -->
                </div>

            </div>
        </div>

    </div>
    <!-- END CONTAINER -->
</section>
<!-- END FOURTH SECTION -->

<section id="fourth-and-half-section">
    <!-- fourth and half section -->
    <div class="row">
        <!-- START ROW -->
        <div class="col-md-12">
            <div class="tittle-page">
                <!-- TITTLE -->
                Nossos cliente
                <span>Dolorem dolores ducimus eaque eligendi eos est ex fugiat harum iure.</span>
            </div>
            <div class="col-md-12">
                <div class="carousel slide clients-side  wow fadeIn" data-wow-delay="0.3s" data-ride="carousel" data-type="multi" data-interval="3000" id="our-clients">
                    <div class="carousel-inner">

                        <div class="item active">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="cients-holder">
                                    <div class="client"><img src="img/p-1.jpg" alt="client photo" /></div>
                                    <div class="client-info">
                                        <h5>nedjai mohamed<span>CEO - bloodi</span></h5></div>
                                    <div class="client-comm">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis,
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="cients-holder">
                                    <div class="client"><img src="img/p-2.jpg" alt="client photo" /></div>
                                    <div class="client-info">
                                        <h5>tablet mohamed<span>CEO - bloodi</span></h5></div>
                                    <div class="client-comm">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis,
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="cients-holder">
                                    <div class="client"><img src="img/p-3.jpg" alt="client photo" /></div>
                                    <div class="client-info">
                                        <h5>aisa njaar<span>CEO - bloodi</span></h5></div>
                                    <div class="client-comm">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis,
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="cients-holder">
                                    <div class="client"><img src="img/p-4.jpg" alt="client photo" /></div>
                                    <div class="client-info">
                                        <h5>smaah bnlawal<span>CEO - bloodi</span></h5></div>
                                    <div class="client-comm">
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis,
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="container hide">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sponsor-place">
                            <div class="col-md-2">
                                <a class="wow fadeIn" data-wow-delay="0.1s" data-toggle="tooltip" data-placement="top" title="evento" href="../index.html#"><img src="img/sponsor.png" alt="sponsor" /> </a>
                            </div>

                            <div class="col-md-2">
                                <a class="wow fadeIn" data-wow-delay="0.3s" data-toggle="tooltip" data-placement="top" title="evento" href="../index.html#"><img src="img/sponsor.png" alt="sponsor" /> </a>
                            </div>

                            <div class="col-md-2">
                                <a class="wow fadeIn" data-wow-delay="0.5s" data-toggle="tooltip" data-placement="top" title="evento" href="../index.html#"><img src="img/sponsor.png" alt="sponsor" /> </a>
                            </div>

                            <div class="col-md-2">
                                <a class="wow fadeIn" data-wow-delay="0.7s" data-toggle="tooltip" data-placement="top" title="evento" href="../index.html#"><img src="img/sponsor.png" alt="sponsor" /> </a>
                            </div>

                            <div class="col-md-2">
                                <a class="wow fadeIn" data-wow-delay="0.9s" data-toggle="tooltip" data-placement="top" title="evento" href="../index.html#"><img src="img/sponsor.png" alt="sponsor" /> </a>
                            </div>

                            <div class="col-md-2">
                                <a class="wow fadeIn" data-wow-delay="1.1s" data-toggle="tooltip" data-placement="top" title="evento" href="../index.html#"><img src="img/sponsor.png" alt="sponsor" /> </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    <!-- END ROW -->
</section>
<!-- END FOURTH SECTION -->

<section id="fifth-section">
    <!-- fifth section -->
    <div class="container">
        <!-- START CONTAINER -->
        <div class="row">
            <!-- START ROW -->
            <div class="col-md-12">

                <div class="col-md-7">
                    <div class="help-center">
                        <h3 class="wow fadeIn" data-wow-delay="0.1s">help & support <span>Questions already asked</span></h3> <!-- TITTLE -->

                        <ul>
                            <li><a class="wow fadeIn" data-wow-delay="0.1s" href="../index.html#"><span>+</span> Lorem ipsum dolor sit amet, consectetuer </a></li>
                            <li><a class="wow fadeIn" data-wow-delay="0.2s" href="../index.html#"><span>+</span> Lorem ipsum dolor sit amet, consectetuer Lorem ipsum dolor sit amet, consectetuer Lorem ipsum dolor sit amet, consectetuer</a></li>
                            <li><a class="wow fadeIn" data-wow-delay="0.3s" href="../index.html#"><span>+</span> Lorem ipsum  </a></li>
                            <li><a class="wow fadeIn" data-wow-delay="0.4s" href="../index.html#"><span>+</span> Lorem ipsum dolor sit amet</a></li>
                        </ul>
                        <div class="btn-holder wow fadeIn" data-wow-delay="0.6s"><a class="rg-bttn bluebk" style="text-transform: uppercase; border-radius: 50px;" href="../index.html#">visit help center</a></div>
                    </div>
                </div>

                <div class="col-md-5">
                    <img class="wow fadeIn" data-wow-delay="0.4s" src="img/support.png" alt="support" /><!-- IMAGE -->
                </div>

            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</section>
<!-- END FIFTH SECTION -->

<section id="our-place">
    <!-- OUR PLACE SECTION -->
    <div class="container">
        <!-- CONTAINER ROW -->
        <div class="row">
            <!-- START ROW -->
            <div class="place-tittle">
                <h3>Contate-nos</h3><!-- TITTLE -->
            </div>

            <div class="col-xs-12"><!-- START COLL -->

                <div class="col-md-4">
                    <div class="our-plce-tittle wow fadeIn" data-wow-delay="0.2s">
                        <img src="img/plain.png" alt="paper plain" />
                        <h3>Cachoeira do Sul</h3>

                        <div class="address">
                            <span>Aparácio Borges, 375</span>
                            <span>CEP: 96501-070</span>
                        </div>

                        <div class="mobandmail">
                            <span>(51)3724-4461</span>
                            <span>sac@ivocs.com.br</span>
                        </div>

                    </div>
                </div>


                <div class="col-md-4">
                    <div class="our-plce-tittle wow fadeIn" data-wow-delay="0.5s">
                        <img src="img/plain.png" alt="paper plain" />
                        <h3>Restinga Seca</h3>

                        <div class="address">
                            <span>Borges de Medeiros, 30</span>
                            <span>CEP: 97200-000</span>
                        </div>

                        <div class="mobandmail">
                            <span>(55)9 9908-8472</span>
                            <span>atendimento.restinga@ivocs.com.br</span>
                        </div>

                    </div>
                </div>


                <div class="col-md-4">
                    <div class="our-plce-tittle wow fadeIn" data-wow-delay="0.5s">
                        <img src="img/plain.png" alt="paper plain" />
                        <h3>Agudo</h3>

                        <div class="address">
                            <span>Av. Tiradentes, 1387</span>
                            <span>CEP: 96540-000</span>
                        </div>

                        <div class="mobandmail">
                            <span>(55) 9 9908-847</span>
                            <span>atendimento.agudo@ivocs.com.br</span>
                        </div>

                    </div>
                </div>

            </div><!-- END COLL -->
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</section>
<!-- END OUR PLACE SECTION -->

<section id="social-icons">
    <!-- START SOCIAL ICON SECTION -->
    <div class="container">
        <!-- START CONTAINER -->
        <div class="row">
            <!-- START ROW -->
            <div class="col-md-12 wow fadeIn" data-wow-delay="0.4s">
                <ul>
                    <li><a class="twico" href="../index.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <!-- TWITTER ICON -->
                    <li><a class="fbico" href="https://www.facebook.com/ProvedorDeInternetIvocs/?fref=ts"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <!-- FACEBOOK ICON -->
                    <li><a class="inico" href="../index.html#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    <!-- INSTAGRAM ICON -->
                    <li><a class="goico" href="../index.html#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <!-- GOOGLE PLUS ICON -->
                    <li><a class="drico" href="../index.html#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <!-- DRIBBBLE ICON -->
                    <li><a class="yoico" href="../index.html#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    <!-- YOUTUBE ICON -->
                </ul>
            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->

</section>
<!-- END SOCIAL ICON SECTION -->

<section id="contact-form" class="wow fadeIn" data-wow-delay="0.3s">
    <!-- CONTACT FORM -->
    <div class="container">
        <!-- START CONTAINER -->
        <div class="row">
            <!-- START ROW -->
            <div class="col-md-10 col-md-push-1">
                <h3 class="contact-tittle">Ainda com dúvidas?</h3>
                <!-- TITTLE -->
                <div id="form-messages"></div>
                <!-- FORM MESSAGE -->

                <form id="ajax-contact" method="post" action="http://coodiv.net/project/cabhost/mailer.php">
                    <!-- START FORM -->
                    <div class="col-md-5">
                        <div class="field">
                            <label for="name">Name:</label>
                            <input type="text" id="name" name="name" placeholder="Name *" required>
                            <!-- NAME INPUT -->
                        </div>

                        <div class="field">
                            <label for="email">Email:</label>
                            <input type="email" id="email" name="email" placeholder="Email *" required>
                            <!-- EMAIL INPUT -->
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="field">
                            <label for="message">Message:</label>
                            <textarea id="message" name="message" placeholder="Message *" required></textarea>
                            <!-- TEXTAREA -->
                        </div>
                    </div>

                    <div class="btn-holder-contect">
                        <button type="submit">Enviar</button>
                        <!-- SEND BUTTON -->
                    </div>
                </form>
                <!-- END FORM -->
            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</section>
<!-- END CONTACT FORM -->

<footer>
    <!-- START FOOTER -->
    <div class="container">
        <!-- START CONTAINER -->
        <div class="row">
            <!-- START ROW -->
            <div class="col-md-12">

                <div class="contact-info">
                    <a href="../index.html#">sac@ivocs.com.br</a> - (51)3724-4461
                    <!-- YOUR EMAIL AND PHONE HERE -->
                </div>
                <div class="copyright-footer">
                    © <?php echo date('Y');?> - Todos os direitos reservados.
                    <!-- YOUR COPYRIGHT HERE -->
                </div>
            </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTAINER -->
</footer>
<!-- END FOOTER -->

<!-- core JavaScript -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/Particleground.js"></script>
<script src="js/sticky.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/contact.js"></script>
<script src="js/multi-carousel.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/functions.js"></script>
</body>
<!-- END BODY -->

</html>
<!-- END HTML -->