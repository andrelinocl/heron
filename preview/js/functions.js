$(document).ready(function() {
    /*Preloader*/
    $('.se-pre-con').delay(1300).fadeOut('slow');
    /*tooltip*/
	$('[data-toggle="tooltip"]').tooltip();
	/*wow fade*/
	new WOW().init();
	/*go top*/
	$('body').append('<div id="toTop" class="btn"><span class="fa fa-chevron-up"></span></div>');
    	$(window).scroll(function () {
			if ($(this).scrollTop() != 0) {
				$('#toTop').fadeIn();
			} else {
				$('#toTop').fadeOut();
			}
		}); 
    $('#toTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
	/*smooth Scroll*/
	smoothScroll.init();
});